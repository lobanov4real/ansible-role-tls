[![Project Status: Active – The project has reached a stable, usable state and is being actively developed.](https://www.repostatus.org/badges/latest/active.svg)](https://www.repostatus.org/#active)
[![Ansible Lint](https://img.shields.io/badge/Ansible-Lint-purple.svg)](https://ansible.readthedocs.io/projects/lint/)
[![Molecule test](https://img.shields.io/badge/Molecule-Test-purple.svg)](https://ansible.readthedocs.io/projects/molecule/)
[![GitLab CI](https://gitlab.com/lobanov4real/ansible-role-tls/badges/main/pipeline.svg)](https://gitlab.com/lobanov4real/ansible-role-tls/-/pipelines)
[![License](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/license/mit/)

# TLS

Generate OWNCA certificate ans issue TLS ceriticate for web-server signed by OWNCA.

## Description

1. Generate OWNCA certificate.
2. Generate TLS certificate signed by OWNCA certificate.
3. Add OWNCA certificate to Trusted Root Store.
4. Verify TLS certificate by openssl (molecule task).

## Requirements

### Common

- Openssl.
- Python3 and modules:
  - pip,
  - packaging,
  - cryptjgrathy.

### Ansible

- This role is developed and tested with [maintained](https://docs.ansible.com/ansible/latest/reference_appendices/release_and_maintenance.html#) versions of `ansible-core` **2.16**.
- When using `ansible-core`, you will also need to install the following collections:

  ```yaml
  ---
  collections:
    - name: ansible.posix
      version: 1.5.4
    - name: community.general
      version: 6.4.0
    - name: community.crypto
      version: 2.14.7
    - name: community.docker
      version: 3.4.7
  ```

- You will need to run this role as a root user using Ansible's `become` parameter. Make sure you have set up the appropriate permissions on your target hosts.
- Instructions on how to install Ansible can be found in the [Ansible website](https://docs.ansible.com/ansible/latest/installation_guide/index.html).

### Jinja2

- This role uses Jinja2 templates. `ansible-core` installs Jinja2 by default, but depending on your install and/or upgrade path, you might be running an outdated version of Jinja2. The minimum version of Jinja2 required for the role to properly function is **3.1**.
- Instructions on how to install Jinja2 can be found in the [Jinja2 website](https://jinja.palletsprojects.com/en/3.1.x/intro/#installation).

### Molecule (optional)

- Molecule is used to test the various functionalities of the role. The recommended version of Molecule to test this role is **6.x.x**.
- Instructions on how to install Molecule can be found in the [Molecule website](https://ansible.readthedocs.io/projects/molecule/installation/). You will also need to install the Molecule Docker driver.

## Installation

### Git

To pull the latest edge commit of the role from GitLab, use:

```shell
git clone https://gitlab.com/lobanov4real/ansible-role-tls.git
```

## Platforms

The TLS Ansible role tested in next platforms:

```yaml
Debian:
  - bullseye (11)
Oracle Linux:
  - 9
Red Hat:
  - 9
SUSE:
  - 15.5
Amazon Linux:
  - 2
AlmaLinux:
  - 8
Ubuntu:
  - jammy (22.04)
```

## Role Variables

The defaults variables can be found in the defaults/main.yml:

```yaml
---
### Common
tls_tmp_path: /var/tmp

### OWNCA certificate
# Generate private key for OWNCA certificate
ownca_key_name: ownca_key
ownca_key_size: "4096"
ownca_privatekey_passphrase: "PaSSword1213"

# Generate CSR (Certificate Signing Request) for OWMCA Certificate
ownca_csr_name: ownca_csr
ownca_common_name: "ownca"
ownca_csr_country_name: RU
ownca_csr_email_address: demo@acme.com
ownca_csr_locality_name: Moscow
ownca_csr_organization_name: ACME
ownca_csr_organizational_unit_name: IT
ownca_csr_state_or_province_name: "Moscow region"

# Generate OWNCA certificate
ownca_certificate_name: ownca
ownca_selfsigned_not_after_days: "+730d"
ownca_selfsigned_not_before_days: "-2d"

### TLS certificate
# Generate private key for TLS certificate
tls_key_name: tls_key
tls_key_size: "2048"

# Generate CSR (Certificate Signing Request) for TLS Certificate
tls_csr_name: tls_csr
tls_common_name: tls
tls_csr_country_name: RU
tls_csr_email_address: demo@acme.com
tls_csr_locality_name: Moscow
tls_csr_organization_name: ACME
tls_csr_organizational_unit_name: IT
tls_csr_state_or_province_name: "Moscow region"


# Generate TLS certificate
tls_certificate_name: tls
tls_ownca_not_after_days: "+365d"
tls_ownca_not_before_days: "-1d"
```

The preset variables can be found in the vars/main.yml:

```yaml
---
# Common
hostname: "{{ ansible_facts['hostname'] }}"

## OS dependencies

# Alpine dependencies
tls_alpine_dependencies: [ca-certificates, coreutils, openssl, pcre2]

# Debian dependencies
tls_debian_dependencies: [apt-transport-https, ca-certificates, gpg-agent, openssl]

# Red Hat dependencies
tls_redhat_dependencies: [ca-certificates, openssl]

# SLES dependencies
tls_sles_dependencies: [ca-certificates, openssl, ca-certificates-cacert]
```

## Example Playbook

Working functional playbook example can be found in the molecule/converge.yml:

```yaml
---
- name: Converge
  hosts: all
  tasks:
    - name: Generate TLS certificate for web-server
      ansible.builtin.include_role:
        name: lobanov4real.tls
```

## License

MIT
